import { productsLikeReducer } from "../features/Products/Like/like.reducer";
import cartReducer from "./cart/cart.reducer";



const rootReducer = (state = {},action) => {
    return {
        ...state,
        productsLike : productsLikeReducer(state.productsLike,action),
        productsInCart: cartReducer(state.productsInCart,action)
    }
}

export default rootReducer
