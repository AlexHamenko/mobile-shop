import React from 'react'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'
import './cart.css'

import products, { getProductsMap } from './../../Main/Products/products'
import CartTotal from './../../../features/Cart/CartTotal'
import CartProductList from '../../../features/Cart/CartProductList';



const Cart = ({
    productsInCart,
    productsMap=getProductsMap(products)
}) =>
<div className="cart text-center">
    <CartProductList productsInCart={productsInCart}/>
    <CartTotal productsInCart={productsInCart}/>
    <Link to="/cart">Show cart</Link>
</div>
 
const mapStateToProps = (state) => ({
    productsInCart:state.productsInCart,
})


export default connect(
    mapStateToProps,
)(Cart)
