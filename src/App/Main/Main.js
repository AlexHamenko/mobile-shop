import React from 'react'
import {Route} from 'react-router-dom'
import ProductsList from './Products/ProductsList'
import PaymentPage from './PaymentPage/PaymentPage'
import ShippingPage from './ShippingPage/ShippingPage'
import CartPage from './CartPage/CartPage'

 
const Main = () => (
	<main className="main">
		<div className="container">
		<div className="row">
			<div className="col-lg-3">
				Filter
			</div>
			<div className="col-lg-9">
				<Route exact path="/" component={ProductsList }/>
				<Route  path="/products" component={ProductsList }/>	
				<Route 	path="/cart" component={CartPage }/>
				<Route  path="/payment" component={PaymentPage}/>
				<Route  path="/shipping" component={ShippingPage}/>
				
			</div>
		</div>
		</div>
	</main>
)

export default Main
