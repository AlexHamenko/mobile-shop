const products = [
    {
        id:1,
        name:"iPhone 7", 
        description:"This is iPhone 7",
        type:"phone",
        capacity:64,
        price:600,
        image: "images/products/iphone.png",
        category:"top",
    },
    {
        id:"2",
        name:"iPhone 8",
        // description:"This is iPhone 8",
        type:"phone",
        capacity:528,
        price:1600,
        image: "images/products/ipod.png",
        
    },
    {
        id:3,
        name:"iPhone X",
        description:"This is iPhone X",
        type:"phone",
        capacity:264,
        price:800,
        image: "images/products/iphone.png",
        category:"top",
    },
    {
        id:4,
        name:"iPhone 8 plus",
        description:"This is iPhone 8 plus",
        type:"phone",
        capacity:128,
        price:1000,
        image: "images/products/ipod.png",
    },
]


export const getProductsMap = (array) => {
    return array.reduce((map,product)=>({
        ...map,
        [product.id]:product,
    }),{})
}

export default products
