import React,{ Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'

import './ProductListItem.css'
import QuantityInput from '../../../features/Quantity/QuantityInput';
 
class ProductListItem extends Component {


    static propTypes = {
        name: PropTypes.string.isRequired,
        description:PropTypes.string,
        type:PropTypes.string,
        capacity:PropTypes.number,
        price:PropTypes.number.isRequired,

    }

    static defaultProps = {
        isLiked:false
    }

    state = {
        productCount: 1,
    }

    onIncrementClick = () => {
        this.setState((prevState)=>({
            productCount:prevState.productCount + 1
        }))
    }
    onDecrementClick = () => {
        this.setState((prevState)=>({
            productCount:prevState.productCount - 1
        }))
    }
    renderProductQuantity () {
        return (
            <QuantityInput
                productCount={this.state.productCount}
                onIncrementClick={this.onIncrementClick}
                onDecrementClick={this.onDecrementClick}
                minValue={1}
            />
        )
    }

    renderLike = () =>{
        if(this.props.isLiked) {
            this.props.onDislikeClick(this.props.id)
        } else {
            this.props.onLikeClick(this.props.id)
        }
    }
    
    render() {
        const {
            name,
            image,
            description = "No description ...",
            capacity,
            type,
            price,
            addProductToCart,
            id,
            isLiked,
        } = this.props
        
        return (
            <div className="product-list-item">
                <div className="product-image">
                    <img src={image} alt=""/>
                </div>
                <button onClick={this.renderLike}>
                    {isLiked ? <span>&#9829;</span>: <span>&#9825;</span>}
                </button>
                <div className="product-title">{name}</div>
                <div className="product-description">{description}</div>
                <div className="product-type">Type : {type}</div>
                <div className="product-capacity">Capacity: {capacity} Gb</div>
                { this.renderProductQuantity()}
                <div className="product-price">$ {price}</div>
                <button 
                    className="btn-add-to-cart"
                    onClick={()=>addProductToCart(id,this.state.productCount)}
                    
                    >Add to Cart</button>
            </div>
        )
    }
}

const mapStateToProps = (state,props)=>({
    isLiked:state.productsLike[props.id],
})

const mapDispatchToProps = (dispatch) => ({
    onLikeClick:(id) => dispatch({
        type: "LIKE",
        id:id
    }),
    onDislikeClick:(id) => dispatch({
        type: "DISLIKE",
        id:id
    }),
    addProductToCart:(id,count) => dispatch({
        type:"ADD_PRODUCT_TO_CART",
        id:id,
        count:count
    })
})
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProductListItem)