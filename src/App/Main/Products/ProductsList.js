import React from 'react'
import ProductListItem from './ProductListItem'

import products from './products'
 
const ProductsList = () => (
    <div className="products-list">
        <h1 className="title-page text-center">Products List</h1>
        <div className="row">
            {
                products.map(({
                    id,
                    name,
                    capacity,
                    description,
                    type,
                    price,
                    image
                }) => (
                    <div className="col-lg-6" key={id}>
                        <ProductListItem
                            name={name} 
                            description={description}
                            type={type}
                            capacity={capacity}
                            price={price}
                            image={image}
                            id={id}
                        />
                    </div>
                ))
            }

            
           
        </div>
    </div>
)

export default ProductsList
