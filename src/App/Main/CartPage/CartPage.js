import React from 'react'
import {connect} from 'react-redux'

import products, { getProductsMap } from './../Products/products'

import CartTotal from './../../../features/Cart/CartTotal'
import CartProductList from '../../../features/Cart/CartProductList';
import CartProductListItemExtended from '../../../features/Cart/CartProductListItemExtended';

const CartPage = ({
    productsInCart,
    productsMap = getProductsMap(products),

}) => {
    return (
        <div>
            <h1>Cart Page</h1>
            <div className="cart text-center">
                <CartProductList 
                    productsInCart={productsInCart}
                    ListItemComponent={CartProductListItemExtended}
                    />
                <CartTotal productsInCart={productsInCart}/>
            </div>
        </div>
       
    )
}
const mapStateToProps = (state) => ({
    productsInCart:state.productsInCart,
})


export default connect(
    mapStateToProps,
)(CartPage)
