import React from 'react'

import './../common/reset.css'
import './../common/base.css'

import Header from './Header/Header'
import Main from './Main/Main'
import Footer from './Footer/Footer'

const App = () => (
	<div>
		<Header/>
		<Main/>
		<Footer/>
	</div>
)




export default App
